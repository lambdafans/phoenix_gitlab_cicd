defmodule HelloGitlabCi.Repo do
  use Ecto.Repo,
    otp_app: :hello_gitlab_ci,
    adapter: Ecto.Adapters.Postgres
end
