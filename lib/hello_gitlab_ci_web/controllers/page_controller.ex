defmodule HelloGitlabCiWeb.PageController do
  use HelloGitlabCiWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
