# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :hello_gitlab_ci,
  ecto_repos: [HelloGitlabCi.Repo]

# Configures the endpoint
config :hello_gitlab_ci, HelloGitlabCiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "hRSPlkrMeaAUMYCp0xMFhZ7+RC31iOOVM7HIly4SZFjeMGgQRDbcqUMaSAbo27e8",
  render_errors: [view: HelloGitlabCiWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: HelloGitlabCi.PubSub,
  live_view: [signing_salt: "FYo1HD2O"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
